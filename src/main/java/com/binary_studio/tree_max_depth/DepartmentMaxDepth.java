package com.binary_studio.tree_max_depth;

import java.util.ArrayDeque;
import java.util.Objects;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer recMaxDepth(Department rootDepartment) {

		if (rootDepartment == null) {
			return 0;
		}
		int depth = 0;

		for (Department department : rootDepartment.subDepartments) {
			depth = Math.max(depth, calculateMaxDepth(department));
		}
		return depth + 1;
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		ArrayDeque<Department> queue = new ArrayDeque<>();
		if (rootDepartment != null) {
			queue.offer(rootDepartment);
		}

		int depth = 0;
		while (!queue.isEmpty()) {
			for (Department department : queue) {
				department.subDepartments.stream().filter(Objects::nonNull).forEach(queue::offer);
				queue.poll();
			}
			depth++;
		}

		return depth;
	}

}
