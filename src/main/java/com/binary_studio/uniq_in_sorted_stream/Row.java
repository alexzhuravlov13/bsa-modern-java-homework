package com.binary_studio.uniq_in_sorted_stream;

public final class Row<RowData> {

	private final Long id;

	public Row(Long id) {
		this.id = id;
	}

	public Long getPrimaryId() {
		return this.id;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Row<RowData> rowObj = (Row<RowData>) obj;
		return this.getPrimaryId().equals(rowObj.getPrimaryId());
	}

}
