package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public class AttackableShip implements Attackable {

	private CombatReadyShip ship;

	private AttackableShip() {
	}

	public AttackableShip(CombatReadyShip ship) {
		this.ship = ship;
	}

	public CombatReadyShip getShip() {
		return this.ship;
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getCurrentSpeed();
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

}
