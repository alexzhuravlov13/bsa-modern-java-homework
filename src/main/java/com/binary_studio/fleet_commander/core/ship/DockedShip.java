package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DockedShip implements ModularVessel {

	private static final Logger DOCKEDSHIP_LOGGER = LoggerFactory.getLogger(DockedShip.class);

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger speed;

	private PositiveInteger size;

	public DockedShip() {
	}

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger pg,
			PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		DOCKEDSHIP_LOGGER.info("#DockedShip# Construction complete");
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (!pgModuleIsEmpty()) {
			if (subsystem != null) {
				int missingPowergrid = getMissingPgValue(subsystem.getPowerGridConsumption());
				if (isPgValueIsNotEnough(subsystem.getPowerGridConsumption())) {
					throw new InsufficientPowergridException(missingPowergrid);
				}
				DOCKEDSHIP_LOGGER.info("#DockedShip# Attack subsystem fitted");
				usingPg(subsystem.getPowerGridConsumption());
			}
			else {
				DOCKEDSHIP_LOGGER.info("#DockedShip# Attack subsystem unfitted");
			}
			this.attackSubsystem = subsystem;

		}
		else {
			throw new InsufficientPowergridException(0);
		}

	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (!pgModuleIsEmpty()) {
			if (subsystem != null) {
				int missingPgValue = getMissingPgValue(subsystem.getPowerGridConsumption());
				if (isPgValueIsNotEnough(subsystem.getPowerGridConsumption())) {
					throw new InsufficientPowergridException(missingPgValue);
				}
				DOCKEDSHIP_LOGGER.info("#DockedShip# Defencive subsystem fitted");
				usingPg(subsystem.getPowerGridConsumption());
			}
			else {
				DOCKEDSHIP_LOGGER.info("#DockedShip# Defencive subsystem unfitted");
			}
			this.defenciveSubsystem = subsystem;

		}
		else {
			throw new InsufficientPowergridException(0);
		}

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		boolean emptyAttackSubsystem = this.attackSubsystem == null;
		boolean emptyDefenciveSubsystem = this.defenciveSubsystem == null;
		if (emptyAttackSubsystem) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (emptyDefenciveSubsystem) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (emptyAttackSubsystem && emptyDefenciveSubsystem) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		DOCKEDSHIP_LOGGER.info("#DockedShip# Unlocked, ready to combat!");
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitor, this.capacitorRegeneration,
				this.pg, this.attackSubsystem, this.defenciveSubsystem, this.speed, this.size);
	}

	private boolean isPgValueIsNotEnough(PositiveInteger powerGridConsumption) {
		return this.pg.value() < powerGridConsumption.value();
	}

	private int getMissingPgValue(PositiveInteger powerGridConsumption) {
		return powerGridConsumption.value() - this.pg.value();
	}

	private boolean pgModuleIsEmpty() throws InsufficientPowergridException {
		return this.pg == null;
	}

	private void usingPg(PositiveInteger powerGridConsumption) {
		this.pg = PositiveInteger.of(this.pg.value() - powerGridConsumption.value());
	}

}
