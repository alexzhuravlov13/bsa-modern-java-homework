package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class CombatReadyShip implements CombatReadyVessel {

	private static final Logger COMBAT_SHIP_LOGGER = LoggerFactory.getLogger(DefenciveSubsystemImpl.class);

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	private PositiveInteger capacitor;

	private PositiveInteger currentCapacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger speed;

	private PositiveInteger size;

	public CombatReadyShip() {
	}

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.speed = speed;
		this.size = size;
		this.currentHullHP = hullHP;
		this.currentShieldHP = shieldHP;
		this.currentCapacitor = capacitor;
	}

	@Override
	public void endTurn() {
		rechargeCapacitor();

	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {

		AttackAction attackAction = null;
		if (isEnoughCapacitorValue(this.attackSubsystem.getCapacitorConsumption())) {
			usingCapacitor(this.attackSubsystem.getCapacitorConsumption());
			PositiveInteger damage = this.attackSubsystem.attack(target);
			attackAction = new AttackAction(damage, this, target, this.attackSubsystem);
		}

		return Optional.ofNullable(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackableShip target = new AttackableShip((CombatReadyShip) attack.target);
		CombatReadyShip ship = target.getShip();
		AttackAction attackAction = ship.defenciveSubsystem.reduceDamage(attack);
		Integer damageReceived = attackAction.damage.value();
		int shieldAfterDamage = this.currentShieldHP.value() - damageReceived;
		if (shieldAfterDamage < 0) {
			this.currentShieldHP = PositiveInteger.of(0);
			int hullHpAfterDamage = this.currentHullHP.value() + shieldAfterDamage;
			if (hullHpAfterDamage <= 0) {
				COMBAT_SHIP_LOGGER.info("SHIP WAS DESTROYED!");
				return new AttackResult.Destroyed();
			}
			else {
				this.currentHullHP = PositiveInteger.of(hullHpAfterDamage);
			}
		}
		else {
			this.currentShieldHP = PositiveInteger.of(shieldAfterDamage);
		}
		return new AttackResult.DamageRecived(attackAction.weapon, PositiveInteger.of(damageReceived), target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		RegenerateAction regenerate = null;
		if (isEnoughCapacitorValue(this.defenciveSubsystem.getCapacitorConsumption())) {
			usingCapacitor(this.defenciveSubsystem.getCapacitorConsumption());

			PositiveInteger shieldHPRegenerated = this.defenciveSubsystem.regenerate().shieldHPRegenerated;
			PositiveInteger hullHPRegenerated = this.defenciveSubsystem.regenerate().hullHPRegenerated;

			PositiveInteger neededShieldHp = PositiveInteger
					.ofMinus(this.shieldHP.value() - this.currentShieldHP.value());
			PositiveInteger neededHullHp = PositiveInteger.ofMinus(this.hullHP.value() - this.currentHullHP.value());

			if (neededShieldHp.value() < shieldHPRegenerated.value()) {
				shieldHPRegenerated = neededShieldHp;
			}
			if (neededHullHp.value() < hullHPRegenerated.value()) {
				hullHPRegenerated = neededHullHp;
			}

			this.currentShieldHP = PositiveInteger.of(this.currentShieldHP.value() + shieldHPRegenerated.value());
			this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() + hullHPRegenerated.value());
			regenerate = new RegenerateAction(shieldHPRegenerated, hullHPRegenerated);
		}

		return Optional.ofNullable(regenerate);
	}

	private void usingCapacitor(PositiveInteger capacitorConsumption) {
		this.currentCapacitor = PositiveInteger.of(this.currentCapacitor.value() - capacitorConsumption.value());
	}

	private boolean isEnoughCapacitorValue(PositiveInteger capacitorConsumption) {
		return this.currentCapacitor.value() >= capacitorConsumption.value();
	}

	private void rechargeCapacitor() {
		PositiveInteger neededCapacity = PositiveInteger
				.ofMinus(this.capacitor.value() - this.currentCapacitor.value());
		if (neededCapacity.value() <= this.capacitorRegeneration.value()) {
			this.currentCapacitor = PositiveInteger.of(this.currentCapacitor.value() + neededCapacity.value());
		}
		else {
			this.currentCapacitor = PositiveInteger
					.of(this.currentCapacitor.value() + this.capacitorRegeneration.value());
		}
	}

}
