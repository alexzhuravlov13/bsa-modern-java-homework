package com.binary_studio.fleet_commander.core.subsystems;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private static final Logger ATTACK_SUBSYSTEM_LOGGER = LoggerFactory.getLogger(AttackSubsystemImpl.class);

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public AttackSubsystemImpl() {
	}

	public AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		ATTACK_SUBSYSTEM_LOGGER.info("#AttackSubsystem# Construction complete");
		return new AttackSubsystemImpl(name, baseDamage, optimalSize, optimalSpeed, capacitorConsumption,
				powergridRequirments);

	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		PositiveInteger damage = PositiveInteger.of(0);
		PositiveInteger halfOfDamage = PositiveInteger.of(this.baseDamage.value() / 2);
		double sizeReductionModifier;
		BigDecimal speedReductionModifier;

		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / (double) this.optimalSize.value();
		}
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = new BigDecimal(1);
		}
		else {
			double doubleSpeed = 2 * target.getCurrentSpeed().value();
			speedReductionModifier = new BigDecimal(this.optimalSpeed.value() / doubleSpeed);
			speedReductionModifier = speedReductionModifier.setScale(1, RoundingMode.UP);
		}

		double damageVal = this.baseDamage.value()
				* Math.min(sizeReductionModifier, speedReductionModifier.doubleValue());
		damage = PositiveInteger.of((int) Math.ceil(damageVal));

		ATTACK_SUBSYSTEM_LOGGER.info("#AttackSubsystem# Attacking target \"{}\" size {} speed {}", target.getName(),
				target.getSize().value(), target.getCurrentSpeed().value());
		ATTACK_SUBSYSTEM_LOGGER.info("*PIU PIU PIU* -damage is: {}-", damage.value());
		return damage;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
