package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private static final Logger DEF_SUBSYSTEM_LOGGER = LoggerFactory.getLogger(DefenciveSubsystemImpl.class);

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public DefenciveSubsystemImpl() {
	}

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DEF_SUBSYSTEM_LOGGER.info("#DefenciveSubsystem# Construction complete");
		return new DefenciveSubsystemImpl(name, impactReductionPercent, shieldRegeneration, hullRegeneration,
				capacitorConsumption, powergridConsumption);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger damage;
		double percent = this.impactReduction.value();

		if (percent > 100) {
			if (incomingDamage.damage.value() == PositiveInteger.of(1).value()) {
				damage = PositiveInteger.of(incomingDamage.damage.value());
			}
			else {
				percent = 5;
				damage = PositiveInteger.of((int) Math.ceil(getReduceVal(incomingDamage, percent)));
			}
		}
		else {
			int reduceVal = (int) Math.floor(getReduceVal(incomingDamage, percent));
			damage = PositiveInteger.of(incomingDamage.damage.value() - reduceVal);
		}
		DEF_SUBSYSTEM_LOGGER.info("*BAM BAM BAM*");
		DEF_SUBSYSTEM_LOGGER.info("#DefenciveSubsystem# Shield \"{}\" reduced damage to {} ", this.name,
				damage.value());
		return new AttackAction(damage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	private double getReduceVal(AttackAction incomingDamage, double percent) {
		double reduceValue = incomingDamage.damage.value() * (percent / 100);
		if (reduceValue < 1) {
			return 0;
		}
		else {
			return reduceValue;
		}
	}

	@Override
	public RegenerateAction regenerate() {
		DEF_SUBSYSTEM_LOGGER.info("DefenciveSubsystem# Shield and Hull are regenerated!");
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
